package com.erp.era.dhsmotors.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.erp.era.dhsmotors.R;
import com.erp.era.dhsmotors.fragments.EstimationProductDetailFragment;

public class CostEstimationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cost_estimation);

        Toolbar toolbar = findViewById(R.id.toobar_costestimation);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Estimate Cost");

        Fragment estimationProductDetailFragment = new EstimationProductDetailFragment();

        if (savedInstanceState == null){
            FragmentTransaction estimateProductTransaction = getSupportFragmentManager().beginTransaction();
            estimateProductTransaction.add(R.id.cost_estimation_frame, estimationProductDetailFragment).commit();
        }




    }
}
