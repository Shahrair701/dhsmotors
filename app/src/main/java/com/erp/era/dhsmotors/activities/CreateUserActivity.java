package com.erp.era.dhsmotors.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.erp.era.dhsmotors.R;
import com.erp.era.dhsmotors.fragments.CustomerDetailFragment;

public class CreateUserActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_user);

        Toolbar toolbar = findViewById(R.id.toolbar_user);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("User Registration");

        Fragment customerDetailFragment = new CustomerDetailFragment();

        if (savedInstanceState == null){
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

            ft.add(R.id.customer_information_frame, customerDetailFragment).commit();
        }

    }

}
