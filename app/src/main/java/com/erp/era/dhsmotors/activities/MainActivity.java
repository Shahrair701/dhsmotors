package com.erp.era.dhsmotors.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.erp.era.dhsmotors.R;
import com.erp.era.dhsmotors.util.CheckNetwork;
import com.erp.era.dhsmotors.util.CompanyCodeItems;
import com.erp.era.dhsmotors.util.IPConfigure;
import com.erp.era.dhsmotors.util.JSONParserPost;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity implements TextWatcher, CompoundButton.OnCheckedChangeListener {

    private static final String PREF_NAME = "prefs";
    private static final String KEY_REMEMBER = "remember";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_PASS = "password";
    private static String url_login = IPConfigure.getIP() + "LoginApi";
    private static String URL_DATA;
    EditText password;
    AutoCompleteTextView userName;
    TextView tvCode;
    private CheckBox chk_remember_me;
    private ProgressDialog pDialog;
    Button btnLogin;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Spinner companySpinner;
    String uname;
    String pass;
    String code;
    String usercode;
    String company;
    List<CompanyCodeItems> compList;
    List<String> compCode;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Initialization
        compList = new ArrayList<>();
        compCode = new ArrayList<>();
        userName = findViewById(R.id.tvUserName);
        password = findViewById(R.id.tvPass);
        tvCode = findViewById(R.id.tvCode);
        btnLogin = findViewById(R.id.btnLogin);
        companySpinner = findViewById(R.id.company_name);
        chk_remember_me = findViewById(R.id.chk_remember_me);
        progressBar = findViewById(R.id.pbLoading);

        sharedPreferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        pDialog = new ProgressDialog(this, R.style.Theme_AppCompat_Dialog);

        editor = sharedPreferences.edit();

        if (sharedPreferences.getBoolean(KEY_REMEMBER, false))
            chk_remember_me.setChecked(true);
        else
            chk_remember_me.setChecked(false);

        userName.setText(sharedPreferences.getString(KEY_USERNAME, ""));
        password.setText(sharedPreferences.getString(KEY_PASS, ""));

        userName.addTextChangedListener(this);
        password.addTextChangedListener(this);
        chk_remember_me.setOnCheckedChangeListener(this);

        userName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {

                if (!hasFocus) {
                    findCompanyCode();
                }
            }
        });

        companySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String compCode = companySpinner.getItemAtPosition(companySpinner.getSelectedItemPosition()).toString();
                tvCode.setText(compCode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uname = userName.getText().toString();
                pass = password.getText().toString();
                if (uname.isEmpty()) {
                    userName.requestFocus();

                    Toast.makeText(getApplicationContext(), "Please Enter Your User ID.", Toast.LENGTH_LONG).show();

                } else if (pass.isEmpty()) {
                    password.requestFocus();

                    Toast.makeText(getApplicationContext(), "Please Enter Your Password.", Toast.LENGTH_LONG).show();

                } else if (CheckNetwork.isOnline(MainActivity.this) == false) {


                    //new SweetAlertDialog(Login.this, SweetAlertDialog.ERROR_TYPE);
                    Toast.makeText(getApplicationContext(), "No Internet Connection.", Toast.LENGTH_LONG).show();

                } else {

                    LoginOperation loginOperation = new LoginOperation();
                    loginOperation.execute(uname, pass,code);
                }
            }
        });
    }

    private void managePrefs() {

        if (chk_remember_me.isChecked()) {

            editor.putString(KEY_USERNAME, userName.getText().toString().trim());
            editor.putString(KEY_PASS, password.getText().toString().trim());
            editor.putBoolean(KEY_REMEMBER, true);
            editor.apply();

        } else {

            editor.putBoolean(KEY_REMEMBER, false);
            editor.remove(KEY_PASS);//editor.putString(KEY_PASS,"");
            editor.remove(KEY_USERNAME);//editor.putString(KEY_USERNAME, "");
            editor.apply();
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        managePrefs();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        managePrefs();
    }

    @Override
    public void afterTextChanged(Editable editable) {
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                Intent intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    private void findCompanyCode() {

        uname = userName.getText().toString();

        URL_DATA = IPConfigure.getIP()+"UserAuth?uname="+uname;

        Toast.makeText(this, uname, Toast.LENGTH_SHORT).show();

        progressBar.setVisibility(View.VISIBLE);

        StringRequest stringRequest = new StringRequest(Request.Method.POST,

                URL_DATA, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                progressBar.setVisibility(View.INVISIBLE);
                try {

                    JSONObject jsonObject = new JSONObject(response);

                    JSONArray array = jsonObject.getJSONArray("UserAuth");

                      for (int i = 0; i < array.length(); i++){

                        JSONObject jo = array.getJSONObject(i);

                        CompanyCodeItems companyCodeItems = new CompanyCodeItems(
                                jo.getString("USERCODE"),
                                jo.getString("COMPANY"),
                                jo.getString("CODE"));

                        code = jo.getString("CODE");
                        company = jo.getString("COMPANY");
                        usercode = jo.getString("USERCODE");

                        compList.add(companyCodeItems);
                        compCode.add(code);

                    }

                    companySpinner.setAdapter(new ArrayAdapter<String>(MainActivity.this,
                            android.R.layout.simple_spinner_dropdown_item, compCode));

                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(MainActivity.this, "Error" + error.toString(), Toast.LENGTH_LONG).show();

            }
        });

        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    public class LoginOperation extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
            paramsList.add(new BasicNameValuePair("user", params[0]));
            paramsList.add(new BasicNameValuePair("pass", params[1]));
            paramsList.add(new BasicNameValuePair("compcode", params[2]));

            String myURL = url_login;

            JSONParserPost jsonParserpost = new JSONParserPost();
            JSONObject output = jsonParserpost.makeHttpRequest(myURL, "POST", paramsList);

            if (output != null) {

                JSONArray jsonArray = null;
                String loginStatus = "N";

                try {
                    jsonArray = output.getJSONArray("loginStatus");

                    JSONObject jsonObject = jsonArray.getJSONObject(0);

                    loginStatus = jsonObject.getString("loginStatus");
                    loginStatus = loginStatus.trim();

                } catch (JSONException e) {
                    e.printStackTrace();
                }


                if (loginStatus.equals("Y")) {
                    return "Y";
                } else {
                    return "N";
                }

            } else {
                return "N";
            }

        }

        protected void onProgressUpdate(String... progress) {

        }

        @Override
        protected void onPostExecute(String result) {
            if (result.equals("Y")) {
                uname = userName.getText().toString();
                pDialog.cancel();
                Intent dashboardIntent = new Intent(MainActivity.this, DashBoardActivity.class);
                Bundle extras = new Bundle();
                extras.putString("user", uname);
                extras.putString("code", code);
                dashboardIntent.putExtras(extras);
                startActivity(dashboardIntent);

            } else {
                pDialog.cancel();
                Toast.makeText(MainActivity.this, "Invalid User Name or Password",
                        Toast.LENGTH_LONG).show();
            }

        }
    }

    @Override
    public void onBackPressed() {
        // disable going back to the MainActivity
        moveTaskToBack(true);
    }

}
