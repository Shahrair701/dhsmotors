package com.erp.era.dhsmotors.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.erp.era.dhsmotors.R;

public class NewTokenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_token);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_jobs);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("New Token");
    }
}
