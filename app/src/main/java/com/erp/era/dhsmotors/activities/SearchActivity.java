package com.erp.era.dhsmotors.activities;


import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.erp.era.dhsmotors.R;
import com.erp.era.dhsmotors.adapters.SearchViewAdapter;
import com.erp.era.dhsmotors.util.IPConfigure;
import com.erp.era.dhsmotors.util.SearchItems;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends AppCompatActivity{

    private static String URL_DATA;

    // Declare Variables
    RecyclerView recyclerView;
    SearchViewAdapter adapter;
    ImageView imageView;
    List<SearchItems> searchList;
    SharedPreferences myprefs;
    String searchString;
    String userName;
    String companyCode;
    android.support.v7.widget.Toolbar toolbar;
    MaterialSearchView searchView;
    LinearLayout linearLayoutNoresult;
    ProgressBar searchProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        toolbar = findViewById(R.id.toolbar_search);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Search Services");
        toolbar.setTitleTextColor(Color.parseColor("#FFFFFF"));

        searchView = findViewById(R.id.search_view);

        myprefs= getSharedPreferences("data", Context.MODE_PRIVATE);
        userName= myprefs.getString("username", null);
        companyCode = myprefs.getString("code",null);

        //SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        //View searchView = getLayoutInflater().inflate(R.layout.user_search, null);

        recyclerView = findViewById(R.id.search_recycler);
        searchProgress = findViewById(R.id.search_progress);
        linearLayoutNoresult = findViewById(R.id.linearLayout_no_result);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        searchList = new ArrayList<>();

        searchString = userName;
        loadUrlData();

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {

            @Override
            public void onSearchViewShown() {

            }

            @Override
            public void onSearchViewClosed() {

            }
        });

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchString = query;
                loadUrlData();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

               /* String userInput = newText.toLowerCase();
                List<String> newList = new ArrayList<>();

                for(SearchItems name: searchList) {
                    if (name.toString().toLowerCase().contains(userInput)){
                        newList.add(String.valueOf(name));
                    }
                }

                adapter.notifyDataSetChanged();*/

                return false;
            }
        });

    }

    private void loadUrlData() {

        adapter = new SearchViewAdapter(searchList, getApplicationContext());

        URL_DATA = IPConfigure.getIP() + "SearchApi?search=" + searchString + "&compcode=" + companyCode;

        searchProgress.setVisibility(View.VISIBLE);

        Toast.makeText(this, searchString, Toast.LENGTH_SHORT).show();
        Toast.makeText(this,companyCode, Toast.LENGTH_SHORT).show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                URL_DATA, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                searchProgress.setVisibility(View.INVISIBLE);

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    if (!jsonObject.has("SearchApi")){

                        recyclerView.setVisibility(View.GONE);
                        linearLayoutNoresult.setVisibility(View.VISIBLE);

                    }else {

                        JSONArray array = jsonObject.getJSONArray("SearchApi");

                        Toast.makeText(SearchActivity.this, array.getString(0), Toast.LENGTH_SHORT).show();

                        recyclerView.setVisibility(View.VISIBLE);
                        linearLayoutNoresult.setVisibility(View.GONE);

                        searchList.clear();

                        for (int i = 0; i < array.length(); i++) {

                            JSONObject jo = array.getJSONObject(i);

                            SearchItems searchItems = new SearchItems(

                                    jo.getString("CUSTNAME"),
                                    jo.getString("CUSTMOBL"),
                                    jo.getString("REGISTNO"),
                                    jo.getString("COSTCODE"),
                                    jo.getString("DOCNUMBR"));

                            searchList.add(searchItems);
                            adapter.notifyDataSetChanged();

                        }

                        recyclerView.setAdapter(adapter);

                    }


                } catch (JSONException e) {

                    e.printStackTrace();
                    Toast.makeText(SearchActivity.this, "Error "+ e.toString(), Toast.LENGTH_SHORT).show();
                    searchProgress.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.GONE);
                    linearLayoutNoresult.setVisibility(View.VISIBLE);

                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(SearchActivity.this, "Error" + error.toString(), Toast.LENGTH_SHORT).show();

                searchProgress.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
                linearLayoutNoresult.setVisibility(View.VISIBLE);

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search,menu);
        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);
        return true;
    }

}
