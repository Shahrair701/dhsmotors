package com.erp.era.dhsmotors.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.erp.era.dhsmotors.R;
import com.erp.era.dhsmotors.adapters.SearchViewAdapter;
import com.erp.era.dhsmotors.util.IPConfigure;
import com.erp.era.dhsmotors.util.ServiceItems;
import com.erp.era.dhsmotors.adapters.ServiceActivityAdapter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ServiceActivity extends AppCompatActivity {

    //private static String URL_DATA = IPConfigure.getIP()+"ServiceTasks?costcode=004&docnumbr=SVC00019";


    //private static String URL_DATA;

    ImageView carImage;
    TextView userName;
    TextView userPhn;
    TextView userVin;
    TextView newIssue;
    TextView numOfIssues;


    RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private List<ServiceItems> serviceList;
    String costCode;
    String docNumber;
    String user;
    String phn;
    String vin;
    int counter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);

        recyclerView = findViewById(R.id.service_recycler);
        carImage = findViewById(R.id.service_image);
        userName = findViewById(R.id.user_name);
        userPhn = findViewById(R.id.user_phn);
        userVin = findViewById(R.id.user_vin);
        newIssue = findViewById(R.id.tv_new_issues);
        numOfIssues =  findViewById(R.id.tv_num_issues);
        recyclerView = (RecyclerView) findViewById(R.id.service_recycler);


        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        Intent searchIntent = getIntent();
        user = searchIntent.getStringExtra(SearchViewAdapter.KEY_CUSTNAME);
        phn = searchIntent.getStringExtra(SearchViewAdapter.KEY_CUSTMOBL);
        vin = searchIntent.getStringExtra(SearchViewAdapter.KEY_VIN);
        costCode = searchIntent.getStringExtra(SearchViewAdapter.KEY_COSTCODE);
        docNumber = searchIntent.getStringExtra(SearchViewAdapter.KEY_DOCNUMBER);

        userName.setText(user);
        userPhn.setText(phn);
        userVin.setText(vin);

        serviceList = new ArrayList<>();

        loadUrlData();

    }

    private void loadUrlData() {

        String URL_DATA = IPConfigure.getIP()+"ServiceTasks?costcode="+costCode+"&docnumbr="+docNumber;

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        //Toast.makeText(this, searchString, Toast.LENGTH_SHORT).show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                URL_DATA, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                progressDialog.dismiss();

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    JSONArray array = jsonObject.getJSONArray("ServiceTasks");

                    serviceList.clear();

                    for (int i = 0; i < array.length(); i++){

                        JSONObject jo = array.getJSONObject(i);

                        ServiceItems serviceItems = new ServiceItems(
                                jo.getString("PARENTS_CODE"),
                                jo.getString("CHILD_CODE"));
                        serviceList.add(serviceItems);

                        counter++;

                    }

                    adapter = new ServiceActivityAdapter(serviceList, getApplicationContext());
                    recyclerView.setAdapter(adapter);

                    numOfIssues.setText(String.valueOf(counter));

                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(ServiceActivity.this, "Error" + error.toString(), Toast.LENGTH_SHORT).show();

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }



}
