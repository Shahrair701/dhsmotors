package com.erp.era.dhsmotors.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.erp.era.dhsmotors.R;
import com.erp.era.dhsmotors.activities.ServiceActivity;
import com.erp.era.dhsmotors.util.SearchItems;

import java.util.List;

public class SearchViewAdapter extends RecyclerView.Adapter<SearchViewAdapter.ViewHolder> {

    public static final String KEY_VIN = "VIN";
    public static final String KEY_CUSTNAME = "CUSTNAME";
    public static final String KEY_CUSTMOBL = "CUSTMOBL";
    public static final String KEY_COSTCODE = "COSTCODE";
    public static final String KEY_DOCNUMBER = "DOCNUMBR";

    private List<SearchItems> searchLists;
    private Context context;
    LayoutInflater inflater;

    public SearchViewAdapter(List<SearchItems> searchLists, Context context) {

        this.searchLists = searchLists;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_search_list, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        SearchItems searchItems = searchLists.get(position);
        holder.name.setText(searchItems.getName());
        holder.phnNo.setText(searchItems.getPhnNumber());
        holder.vin.setText(searchItems.getVin());
        //String costCode = searchItems.getCostCode();
        //String docNumber = searchItems.getDoucNum();

        holder.linearLayout.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                SearchItems searchItems1 = searchLists.get(position);

                Intent searchIntent = new Intent(v.getContext(), ServiceActivity.class);

                searchIntent.putExtra(KEY_VIN, searchItems1.getVin());
                searchIntent.putExtra(KEY_CUSTNAME, searchItems1.getName());
                searchIntent.putExtra(KEY_CUSTMOBL, searchItems1.getPhnNumber());
                searchIntent.putExtra(KEY_COSTCODE, searchItems1.getCostCode());
                searchIntent.putExtra(KEY_DOCNUMBER, searchItems1.getDoucNum());

                v.getContext().startActivity(searchIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return searchLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private final TextView name;
        private final TextView phnNo;
        private final TextView vin;
        private final LinearLayout linearLayout;


        public ViewHolder(final View itemView) {
            super(itemView);

            name = ((TextView) itemView.findViewById(R.id.name));
            phnNo = (TextView) itemView.findViewById(R.id.phn_no);
            vin = ((TextView) itemView.findViewById(R.id.vin));
            linearLayout = (LinearLayout) itemView.findViewById(R.id.linearLayout);
        }
    }

    /*public SearchViewAdapter(Context context, List<SearchItems> animalNamesList) {
        mContext = context;
        this.animalNamesList = animalNamesList;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<SearchItems>();
        this.arraylist.addAll(animalNamesList);
    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return animalNamesList.size();
    }

    @Override
    public SearchItems getItem(int position) {
        return animalNamesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.card_search_list, null);
            // Locate the TextViews in listview_item.xml
            holder.name = (TextView) view.findViewById(R.id.name);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        // Set the results into TextViews
        holder.name.setText(animalNamesList.get(position).getAnimalName());
        return view;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        animalNamesList.clear();
        if (charText.length() == 0) {
            animalNamesList.addAll(arraylist);
        } else {
            for (SearchItems wp : arraylist) {
                if (wp.getAnimalName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    animalNamesList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }*/

}
