package com.erp.era.dhsmotors.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.erp.era.dhsmotors.R;
import com.erp.era.dhsmotors.util.ServiceItems;

import java.util.List;

public class ServiceActivityAdapter extends RecyclerView.Adapter<ServiceActivityAdapter.ViewHolder> {


    //define a list from the DevelopersList java class

    private List<ServiceItems> serviceLists;
    private Context context;

    public ServiceActivityAdapter(List<ServiceItems> serviceList, Context context) {

        // generate constructors to initialise the List and Context objects

        this.serviceLists = serviceList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_service_list, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        // this method will bind the data to the ViewHolder from whence it'll be shown to other Views

        final ServiceItems serviceItems = serviceLists.get(position);
        holder.Header.setText(serviceItems.getHeader());
        holder.detail.setText(serviceItems.getDetail());

    }

    @Override
    public int getItemCount() {
        return serviceLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private final TextView Header;
        private final TextView detail;
        public LinearLayout linearLayout;

        public ViewHolder(final View itemView) {

            super(itemView);

            Header = ((TextView) itemView.findViewById(R.id.header));
            detail = (TextView) itemView.findViewById(R.id.detail);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.service_linearlayout);
        }
    }
}
