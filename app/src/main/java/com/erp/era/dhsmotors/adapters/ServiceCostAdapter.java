package com.erp.era.dhsmotors.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.erp.era.dhsmotors.R;
import com.erp.era.dhsmotors.util.ServiceCostItems;

import java.util.List;

public class ServiceCostAdapter extends RecyclerView.Adapter<ServiceCostAdapter.ViewHolder> {

    private List<ServiceCostItems> newServicesList;
    private Context context;
    LayoutInflater inflater;
    String productCode;
    String engine;
    String chassis;
    String registration;

    public ServiceCostAdapter(List<ServiceCostItems> newServicesList, Context context) {
        this.newServicesList = newServicesList;
        this.context = context;
    }

    public ServiceCostAdapter(String productCode, String engine, String chassis, String registration) {

        this.productCode = productCode;
        this.engine = engine;
        this.chassis = chassis;
        this.registration = registration;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_table_list,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        ServiceCostItems serviceCostItems = newServicesList.get(position);

        holder.productCode.setText(serviceCostItems.getProductCode());
        holder.chassis.setText(serviceCostItems.getChassis());
        holder.engineNo.setText(serviceCostItems.getEngine());
        holder.registration.setText(serviceCostItems.getRegistrationNo());

    }

    @Override
    public int getItemCount() {
        return newServicesList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        private final TextView productCode;
        private final TextView chassis;
        private final TextView engineNo;
        private final TextView registration;


        public ViewHolder(final View itemView) {
            super(itemView);

            productCode  =  itemView.findViewById(R.id.tv_product_code);
            chassis =  itemView.findViewById(R.id.tv_chassis);
            engineNo =  itemView.findViewById(R.id.tv_engine);
            registration =  itemView.findViewById(R.id.tv_registration);

        }
    }
}
