package com.erp.era.dhsmotors.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.erp.era.dhsmotors.R;
import com.erp.era.dhsmotors.adapters.ServiceCostAdapter;
import com.erp.era.dhsmotors.util.ServiceCostItems;

import java.util.ArrayList;
import java.util.List;

public class CustomerServiceCostFragment extends Fragment {


    public CustomerServiceCostFragment() {
        // Required empty public constructor
    }

    String productCode;
    String engine;
    String chassis;
    String registration;

    RecyclerView newServiceRecycler;
    Button submitButton;
    EditText etProductCode;
    EditText etEngine;
    EditText etChassis;
    EditText etregistration;

    private List<ServiceCostItems> serviceCostList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_customer_new_service, container, false);

        serviceCostList = new ArrayList<>();
        newServiceRecycler = view.findViewById(R.id.new_service_recycler);
        submitButton = view.findViewById(R.id.bu_add);
        etProductCode = view.findViewById(R.id.et_product_code);
        etEngine = view.findViewById(R.id.et_engine);
        etChassis = view.findViewById(R.id.et_chesis);
        etregistration = view.findViewById(R.id.et_registration);

        newServiceRecycler.setHasFixedSize(true);

        submitButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                populateList();
            }

        });

        // Inflate the layout for this fragment
        return view;

    }

    public void populateList(){

        productCode = etProductCode.getText().toString();
        engine = etEngine.getText().toString();
        chassis = etChassis.getText().toString();
        registration = etregistration.getText().toString();

        ServiceCostItems serviceCostItems = new ServiceCostItems(productCode,engine,chassis,registration);
        serviceCostList.add(serviceCostItems);

        ServiceCostAdapter serviceCostAdapter = new ServiceCostAdapter(serviceCostList, getContext());
        newServiceRecycler.setAdapter(serviceCostAdapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        newServiceRecycler.setLayoutManager(layoutManager);

    }

}
