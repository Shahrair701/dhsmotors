package com.erp.era.dhsmotors.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;

import com.erp.era.dhsmotors.R;

public class CustomerServiceTypeFragment extends Fragment {

    EditText editText;
    TableLayout tableHeader;
    TableLayout tableElement;
    View view;
    int counter = 0;
    AppCompatImageButton addRow;
    AppCompatImageButton removeRow;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_customer_service_type, container, false);

        Button bu_next = view.findViewById(R.id.bu_next);
        addRow = view.findViewById(R.id.add_row);
        removeRow = view.findViewById(R.id.remove_row);

        tableHeader = view.findViewById(R.id.tableLayout_header);
        tableElement = view.findViewById(R.id.tableLayout_element);

        addRow.setOnClickListener(new View.OnClickListener() {

            TableRow.LayoutParams params = new TableRow.LayoutParams(0, dpToPx(50), 1f);

            @Override
            public void onClick(View v) {

                TableRow row = new TableRow(getContext());

                for (int i = 0; i <= 3; i++) {

                    editText = new EditText(getContext());
                    addRow();
                    editText.setLayoutParams(params);

                    row.addView(editText);
                }

                tableElement.addView(row);

                counter++;

                setVisibility();

            }

        });

        removeRow.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                tableElement.removeViewAt(counter);

                counter--;
                setVisibility();

            }
        });


        bu_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.customer_information_frame, new CustomerDetailFragment());
                fragmentTransaction.commit();
            }
        });

        return view;
    }

    public void setVisibility() {

        if (counter >= 1) {

            removeRow.setVisibility(View.VISIBLE);

        } else if (counter < 1) {

            removeRow.setVisibility(View.INVISIBLE);

        }
    }

    public void addRow() {

        final int sdk = android.os.Build.VERSION.SDK_INT;

        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            editText.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.edit_text_background));
            editText.setText("New Row");

        } else {
            editText.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.edit_text_background));
            editText.setText("New Row");

        }

    }

    public int dpToPx(int dp) {
        float density = getContext().getResources()
                .getDisplayMetrics()
                .density;
        return Math.round((float) dp * density);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (view != null) {
            ViewGroup parentViewGroup = (ViewGroup) view.getParent();
            if (parentViewGroup != null) {
                parentViewGroup.removeAllViews();
            }
        }
    }

}
