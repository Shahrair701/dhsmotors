package com.erp.era.dhsmotors.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.erp.era.dhsmotors.activities.CostEstimationActivity;
import com.erp.era.dhsmotors.activities.NewTokenActivity;
import com.erp.era.dhsmotors.adapters.CaptionedImagesAdapter;
import com.erp.era.dhsmotors.util.Dashboard_items;
import com.erp.era.dhsmotors.R;
import com.erp.era.dhsmotors.activities.SearchActivity;
import com.nex3z.notificationbadge.NotificationBadge;

public class DashboardFragment extends Fragment {

    int count = 0;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        RecyclerView dashboardRecycler = (RecyclerView) inflater.inflate(R.layout.fragment_dashboard,
                container, false);

        String[] dashboard_item_names = new String[Dashboard_items.dash_items.length];

        for (int i = 0; i < dashboard_item_names.length; i++) {
            dashboard_item_names[i] = Dashboard_items.dash_items[i].getName();
        }

        int[] captionImages = new int[Dashboard_items.dash_items.length];

        for (int i = 0; i < captionImages.length; i++) {
            captionImages[i] = Dashboard_items.dash_items[i].getImageResourceId();
        }

        CaptionedImagesAdapter adapter = new CaptionedImagesAdapter(dashboard_item_names, captionImages/*, count*/);
        dashboardRecycler.setAdapter(adapter);

        adapter.setListener(new CaptionedImagesAdapter.Listener() {

            public void onClick(int position) {
                Intent intent;
                //intent.putExtra(SearchActivity.EXTRA, position);

                switch (position) {

                    case 0:
                        //mbadge.setNumber(++count);
                        break;
                    case 1:
                        intent = new Intent(getActivity(), NewTokenActivity.class);
                        getActivity().startActivity(intent);
                        break;
                    case 2:
                        intent = new Intent(getActivity(), CostEstimationActivity.class);
                        getActivity().startActivity(intent);
                        break;
                    case 3:
                        //mbadge.setNumber(++count);
                        break;
                    case 4:
                        //mbadge.setNumber(++count);
                        break;
                    case 5:
                        //mbadge.setNumber(++count);
                        break;
                    case 6:
                        //mbadge.setNumber(++count);
                        break;
                    case 7:
                        intent = new Intent(getActivity(), SearchActivity.class);
                        getActivity().startActivity(intent);
                        break;
                }

            }
        });

        return dashboardRecycler;

    }

}
