package com.erp.era.dhsmotors.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.erp.era.dhsmotors.R;

public class EstimationProductDetailFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_estimation_product_detail, container, false);

        Button buNext = v.findViewById(R.id.bu_next);
        Button buAdd = v.findViewById(R.id.bu_add);

        AutoCompleteTextView productCode = v.findViewById(R.id.auto_product_code);
        AutoCompleteTextView name = v.findViewById(R.id.auto_product_name);
        AutoCompleteTextView rate = v.findViewById(R.id.auto_product_rate);
        AutoCompleteTextView quantity = v.findViewById(R.id.auto_product_quantity);
        AutoCompleteTextView discount = v.findViewById(R.id.auto_product_discount);

        String productArray[] = new String[4];
        productArray[0] = "Engine";
        productArray[1] = "Brakes";
        productArray[2] = "Safety";
        productArray[3] = "Oil";

        ArrayAdapter<String> productAdapter= new ArrayAdapter<String>
                (getContext(), android.R.layout.simple_list_item_1,productArray);

        productCode.setAdapter(productAdapter);
        name.setAdapter(productAdapter);
        rate.setAdapter(productAdapter);
        quantity.setAdapter(productAdapter);
        discount.setAdapter(productAdapter);


        buNext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.cost_estimation_frame, new EstimationServiceDetailFragment());
                fragmentTransaction.commit();

            }
        });

        buAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        return v;
    }

}
