package com.erp.era.dhsmotors.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.erp.era.dhsmotors.R;

public class EstimationServiceDetailFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_estimation_service_detail, container, false);

        Button buPrevious = v.findViewById(R.id.bu_previous);


        AutoCompleteTextView serviceName = v.findViewById(R.id.auto_service_name);
        AutoCompleteTextView serviceRate = v.findViewById(R.id.auto_service_rate);
        AutoCompleteTextView serviceDiscount = v.findViewById(R.id.auto_service_discount);
        AutoCompleteTextView serviceHour = v.findViewById(R.id.auto_service_hour);
        AutoCompleteTextView serviceAmount = v.findViewById(R.id.auto_service_amount);

        String serviceArray[] = new String[5];

        serviceArray[0] = "Engine thik kor";
        serviceArray[1] = "Tor Rate koto";
        serviceArray[2] = "Discount de";
        serviceArray[3] = "hour koto";
        serviceArray[4] = "amount koto";

        ArrayAdapter<String> serviceAdapter= new ArrayAdapter<String>
                (getContext(), android.R.layout.simple_list_item_1,serviceArray);

        serviceName.setAdapter(serviceAdapter);
        serviceRate.setAdapter(serviceAdapter);
        serviceDiscount.setAdapter(serviceAdapter);
        serviceHour.setAdapter(serviceAdapter);
        serviceAmount.setAdapter(serviceAdapter);

        buPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.cost_estimation_frame, new EstimationProductDetailFragment());
                fragmentTransaction.commit();

            }
        });

        Button buSubmit = v.findViewById(R.id.bu_submit);

        buSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        return v;
    }

}
