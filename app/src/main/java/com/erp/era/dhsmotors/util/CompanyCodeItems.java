package com.erp.era.dhsmotors.util;

public class CompanyCodeItems {

    private String userName;
    private String code;
    private String company;

    public String getUserName(){
        return userName;
    }

    public String getCompany(){
        return company;
    }

    public String getCode(){
        return code;
    }

    public CompanyCodeItems(String userName, String company, String code) {
        this.userName = userName;
        this.company = company;
        this.code = code;
    }
}
