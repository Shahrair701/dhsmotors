package com.erp.era.dhsmotors.util;

import com.erp.era.dhsmotors.R;

public class Dashboard_items {
    private String name;
    private int imageResourceId;

    public static final Dashboard_items[] dash_items = {
            new Dashboard_items("Appoinment", R.drawable.appoinment),
            new Dashboard_items("New Token", R.drawable.newtoken),
            new Dashboard_items("Estimation", R.drawable.estimation),
            new Dashboard_items("Job Opened", R.drawable.jobopened),
            new Dashboard_items("On-Progress", R.drawable.onprogress),
            new Dashboard_items("Complete", R.drawable.complete),
            new Dashboard_items("Delivered", R.drawable.delivered),
            new Dashboard_items("Job History", R.drawable.jobhistory)
    };

    public Dashboard_items(String name, int imageResourceId) {
        this.name = name;
        this.imageResourceId = imageResourceId;
    }

    public String getName() {
        return name;
    }
    public int getImageResourceId() {
        return imageResourceId;
    }
}
