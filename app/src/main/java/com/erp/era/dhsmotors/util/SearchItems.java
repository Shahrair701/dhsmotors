package com.erp.era.dhsmotors.util;

import android.widget.LinearLayout;

public class SearchItems {

    private String name, phnNumber, vin, costCode, doucNum;
    private LinearLayout linearLayout;

    public SearchItems(String name,String phnNumber, String vin, String costCode, String doucNum) {
        this.name = name;
        this.phnNumber = phnNumber;
        this.vin = vin;
        this.costCode = costCode;
        this.doucNum = doucNum;
    }

    public String getName() {
        return name;
    }

    public String getPhnNumber() {
        return phnNumber;
    }

    public String getVin() {
        return vin;
    }

    public String getCostCode() {
        return  costCode;
    }

    public String getDoucNum() {
        return doucNum;
    }
}
