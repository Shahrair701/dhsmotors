package com.erp.era.dhsmotors.util;

public class ServiceCostItems {

    String productCode;
    String engine;
    String chassis;
    String registrationNo;

    public ServiceCostItems(String productCode, String engine, String chassis, String registrationNo) {

        this.productCode = productCode;
        this.engine = engine;
        this.chassis = chassis;
        this.registrationNo = registrationNo;

    }

    public String getProductCode() {
        return productCode;
    }

    public String getEngine() {
        return engine;
    }

    public String getChassis() {
        return chassis;
    }

    public String getRegistrationNo() {
        return registrationNo;
    }
}
