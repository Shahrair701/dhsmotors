package com.erp.era.dhsmotors.util;

public class ServiceItems {

    private String header;
    private String detail;

    public String getHeader(){
        return header;
    }

    public String getDetail(){
        return detail;
    }

    public ServiceItems(String header, String detail) {
        this.header = header;
        this.detail = detail;
    }
}
